package com.faber.srggatewaypredicate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SrGGatewayPredicateApplication {

    public static void main(String[] args) {
        SpringApplication.run(SrGGatewayPredicateApplication.class, args);
    }

}
