package com.faber.servicefeign.service;

import org.springframework.stereotype.Component;

@Component
public class ScheduleClientOneHystrix implements ScheduleClientOne {

    @Override
    public String sayHiFromClientOne(String name) {
        return "sorry "+name;
    }
}
